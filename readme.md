## Forum API
This is RESTful Forum API with authentication, flexible responses and pagination that allows user to do the following:

- Register
- Login
- Create topics and posts
- Like posts

## APIs

- In the sample-request.docx file, you can find a couple of examples requests. The service accepts and responds in JSON format.


## Instructions

- Download and locate the project anywhere in your filesystem
- run the command "composer install" to install Laravel and other dependencies 
- change the .env file settings according to your configurations
- run the commnad "php artisan migrate" to create and migrate database tables
- run the command "php artisan serve" to start the server
- API is ready to be served

example: 

- Open your REST client (such as POSTMAN) and type in the URL http://localhost:8000/api/register and pass data in the body section.  {"username": "", "email": "", "password": ""}
- To login, pass the data to the API in the following format: {"grant_type":"password","client_id": "", "client_secret": "","username": "","password": "","scope": "*"}
- As we use password grant type authentication for this API, therefore the user should provide the client_id and client_secret as well as part of the request
- To obtain these two parameters, run the command "php artisan passport:install"
- The user receives a token by hitting the login endpoint
- take this token and attach it to the authorization header of the HTTP for the next request to the API